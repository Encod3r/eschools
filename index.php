<!doctype html>
<html lang="en">
  <!-- starting point: https://getbootstrap.com/docs/4.0/examples/checkout/ -->
  
  <?php require_once('src/header.php'); ?>

  <body class="bg-light">
    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      <h5 class="my-0 mr-md-auto font-weight-normal"><a href="#">eSchool</a></h5>
      <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="#">Teacher</a>
        <a class="p-2 text-dark" href="#">Contact</a>
        <a class="p-2 text-dark" href="#">Student</a>
      </nav>
      <a class="btn btn-outline-primary" href="#">Sign up</a>
    </div>

    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">Harness OOP power for eSchools</h1>
      <p class="lead">Quickly build an effective pricing table for your potential customers with this Bootstrap example. It's built with default Bootstrap components and utilities with little customization.</p>
    </div>

    <div class="container">
      <div class="card-deck mb-3 text-center">
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Teacher</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">CRUD</h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>Add new teacher</li>
              <li>Edit teacher (to implement)</li>
              <li>Set teacher as contact</li>
              <li>List teachers (to implement)</li>
              <li>Delete teacher</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-outline-primary">
              <a href="src/route.php?resource=teachers&action=index" style="color:inherit; text-decoration:none">List</a>
            </button>
            <button type="button" class="btn btn-lg btn-block btn-outline-success">
              <a href="src/route.php?resource=teachers&action=store" style="color:inherit; text-decoration:none">Create</a>
            </button>
            <button type="button" class="btn btn-lg btn-block btn-outline-warning">
              <a href="src/route.php?resource=teachers&action=update" style="color:inherit; text-decoration:none">Edit</a>
            </button>
            <button type="button" class="btn btn-lg btn-block btn-outline-danger">
              <a href="src/route.php?resource=teachers&action=delete" style="color:inherit; text-decoration:none">Delete</a>
            </button>
          </div>
        </div>
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Contact</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">CRUD</h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>Add new contact</li>
              <li>Edit contact (to implement)</li>
              <li>Assign a student</li>
              <li>List contacts (to implement)</li>
              <li>Delete contact</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-outline-primary">
              <a href="src/route.php?resource=contacts&action=index" style="color:inherit; text-decoration:none">List</a>
            </button>
            <button type="button" class="btn btn-lg btn-block btn-outline-success">
              <a href="src/route.php?resource=contacts&action=store" style="color:inherit; text-decoration:none">Create</a>
            </button>
            <button type="button" class="btn btn-lg btn-block btn-outline-warning">
              <a href="src/route.php?resource=contacts&action=update" style="color:inherit; text-decoration:none">Edit</a>
            </button>
            <button type="button" class="btn btn-lg btn-block btn-outline-danger">
              <a href="src/route.php?resource=contacts&action=delete" style="color:inherit; text-decoration:none">Delete</a>
            </button>
          </div>
        </div>
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Student</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">CRUD</h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>Add new student</li>
              <li>Edit student (to implement)</li>
              <li>Assign one or more guardians</li>
              <li>List students (to implement)</li>
              <li>Delete student</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-outline-primary">
              <a href="src/route.php?resource=students&action=index" style="color:inherit; text-decoration:none">List</a>
            </button>
            <button type="button" class="btn btn-lg btn-block btn-outline-success">
              <a href="src/route.php?resource=students&action=store" style="color:inherit; text-decoration:none">Create</a>
            </button>
            <button type="button" class="btn btn-lg btn-block btn-outline-warning">
              <a href="src/route.php?resource=students&action=update" style="color:inherit; text-decoration:none">Edit</a>
            </button>
            <button type="button" class="btn btn-lg btn-block btn-outline-danger">
              <a href="src/route.php?resource=students&action=delete" style="color:inherit; text-decoration:none">Delete</a>
            </button>
          </div>
        </div>
      </div>

      <?php require_once('src/footer.php'); ?>

    </div>

    <!-- Optional JavaScript -->
    
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>