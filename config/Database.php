<?php
class Database{
    // specify your own database credentials
    private $host = "127.0.0.1";
    private $db_name = "eschool";
    private $username = "root";
    private $password = "";
    public $conn;
  
    // get the database connection
    public function getConnection()
    {
        try {
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            return $this->conn;
        } catch(PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
        }
    }

    public function closeConnection()
    {
        $this->conn = null;
    }
}
// try {
//     $dbh = new PDO('mysql:host=localhost;dbname=eschool', 'root', '');
//     $result = $dbh->prepare('SELECT * FROM students');
//     $result->execute();
//     var_dump($result->fetch(PDO::FETCH_ASSOC));
// } catch (PDOException $e) {
//     print "Error!: " . $e->getMessage() . "<br/>";
//     die();
// }
?>
