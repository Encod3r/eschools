<?php

$resource = $_GET['resource'];
$action = $_GET['action'];

switch ($resource) {
    case 'teachers':
        switch ($action) {
            case 'index': header('Location: views/teachers/index.php'); break;
            case 'store': header('Location: views/teachers/store.php'); break;
            case 'update': header('Location: views/teachers/update.php'); break;
            case 'delete': header('Location: views/teachers/delete.php'); break;
        };break;
    case 'contacts':
        switch ($action) {
            case 'index': header('Location: views/contacts/index.php'); break;
            case 'store': header('Location: views/contacts/store.php'); break;
            case 'update': header('Location: views/contacts/update.php'); break;
            case 'delete': header('Location: views/contacts/delete.php'); break;
        };break;
    case 'students':
        switch ($action) {
            case 'index': header('Location: views/students/index.php'); break;
            case 'store': header('Location: views/students/store.php'); break;
            case 'update': header('Location: views/students/update.php'); break;
            case 'delete': header('Location: views/students/delete.php'); break;
        };break;
}

echo($resource);

?>