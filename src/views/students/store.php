<!doctype html>
<html lang="en">
  <!-- starting point: https://getbootstrap.com/docs/4.0/examples/checkout/ -->
  
  <?php require_once('../../header.php'); ?>

  <body class="bg-light">
    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      <h5 class="my-0 mr-md-auto font-weight-normal"><a href="/eschools/">eSchool</a></h5>
      <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="#">Teacher</a>
        <a class="p-2 text-dark" href="#">Contact</a>
        <a class="p-2 text-dark" href="#"><strong>Student</strong></a>
      </nav>
      <a class="btn btn-outline-primary" href="#">Sign up</a>
    </div>
    <?php 
      include('../../classes/User.php');
      if (isset($_GET['success'])) {
        echo User::getAlert($_GET['success']);
      }
    ?>
    <div class="container">
      <form role="form" action="../../studentController.php" method="post">
        <div class="form-group">
          <label for="title">Title</label>
          <input type="text" class="form-control" id="title" name="title" minlength="1" maxlength="255" placeholder="Enter your title">
        </div>
        <div class="form-group">
          <label for="first_name">First name</label>
          <input type="text" class="form-control" id="first_name" name="first_name" minlength="1" maxlength="255" placeholder="Enter your first name">
        </div>
        <div class="form-group">
          <label for="last_name">Last name</label>
          <input type="text" class="form-control" id="last_name" name="last_name" minlength="1" maxlength="255" placeholder="Enter your last name">
        </div>
        <!-- Not the best UI. A search bar (with autocomplete) would be much better. -->
        <div class="form-group">
          <label for="contact_id">Guardian</label>
          <select class="custom-select" size="6" multiple name='contact_id[]'>
          <?php
              include('../../../config/Database.php');
              $db = new Database();
              $conn = $db->getConnection();
              $columns = [
                  'id',
                  'first_name',
                  'last_name'
              ];
              $statement = User::index($columns, 'contacts', $conn);
              
              for ($i = 0; $i < $statement->rowCount(); $i++) {
                $result = $statement->fetch(PDO::FETCH_OBJ);
                $name = "$result->first_name $result->last_name";
                echo '<option value="' . $result->id . '">' . $name . '</option>';
              }
            ?>
          </select>
        </div>
        <!-- Not the best UI. A search bar (with autocomplete) would be much better. -->
        <div class="form-group">
          <label for="teacher_id">Teacher</label>
          <select class="custom-select" size="6" name='teacher_id'>
          <?php
              include('../../classes/Teacher.php');
              $statement = Teacher::index($columns, 'contacts', $conn);
              
              for ($i = 0; $i < $statement->rowCount(); $i++) {
                $result = $statement->fetch(PDO::FETCH_OBJ);
                $name = "$result->first_name $result->last_name";
                echo '<option value="' . $result->id . '">' . $name . '</option>';
              }
            ?>
          </select>
        </div>
        <input type="hidden" name="_method" value='post'>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
      <?php require_once('../../footer.php'); ?>
    </div>
   
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!-- Optional JavaScript -->
    <script type='text/javascript' src="../../../js/custom.js"></script>
  </body>
</html>