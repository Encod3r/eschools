<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL);

require_once('../config/Database.php');
require_once('../src/classes/User.php');
require_once("../src/classes/Student.php");

$crud_method = $_POST['_method'];

$db = new Database();
$conn = $db->getConnection();
$student = new Student($conn);

switch ($crud_method) {
    case 'post': storeStudent($student, $db);break;
    case 'update': updateStudent($student, $db);break;
    case 'delete': deleteStudent($student, $db);break;
}

/**
 * Support functions
 */

function storeStudent(Student $student, Database $db)
{
    $fillable = [
        'title' => $_POST['title'],
        'first_name' => $_POST['first_name'],
        'last_name' => $_POST['last_name'],
    ];

    $db->closeConnection();

    if (isset($_POST['contact_id'])) {
        $fillable = array_merge($fillable, ['contact_id' => $_POST['contact_id']]);
    }
    if (isset($_POST['teacher_id'])) {
        $fillable = array_merge($fillable, ['teacher_id' => $_POST['teacher_id']]);
    }

    $student_id = $student->create($fillable);

    if ($student_id > 0) {
        header('Location: ../src/views/students/store.php?action=create&success=true');
    } else {
        header('Location: ../src/views/students/store.php?action=create&success=false');
    }
}

function updateStudent(Student $student, Database $db)
{

}

function deleteStudent(Student $student, Database $db)
{

}


?>