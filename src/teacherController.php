<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL);

require_once('../config/Database.php');
require_once('../src/classes/User.php');
require_once("../src/classes/Teacher.php");

$crud_method = $_POST['_method'];

$db = new Database();
$conn = $db->getConnection();
$teacher = new Teacher($conn);

switch ($crud_method) {
    case 'post': storeTeacher($teacher, $db);break;
    case 'update': updateTeacher($teacher, $db);break;
    case 'delete': deleteTeacher($teacher, $db);break;
}

function storeTeacher(Teacher $teacher, Database $conn)
{
    $teacher_id = $teacher->create([
        'title' => $_POST['title'],
        'first_name' => $_POST['first_name'],
        'last_name' => $_POST['last_name'],
    ]);

    if ($teacher_id > 0) {
        header('Location: ../src/views/teachers/store.php?action=create&success=true');
    } else {
        header('Location: ../src/views/teachers/store.php?action=create&success=false');
    }
}

function updateTeacher(Teacher $teacher, Database $conn)
{

}

function deleteTeacher(Teacher $teacher, Database $conn)
{

}

?>