<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL);

require_once('../config/Database.php');
require_once('../src/classes/User.php');
require_once("../src/classes/Contact.php");

$crud_method = $_POST['_method'];

$db = new Database();
$conn = $db->getConnection();
$contact = new Contact($conn);

switch ($crud_method) {
    case 'post': storeContact($contact, $db);break;
    case 'update': updateContact($contact, $db);break;
    case 'delete': deleteContact($contact, $db);break;
}

/**
 * Support functions
 */

function storeContact(Contact $contact, Database $db)
{
    $fillable = [
        'title' => $_POST['title'],
        'first_name' => $_POST['first_name'],
        'last_name' => $_POST['last_name'],
        'email' => $_POST['email'],
        'mobile_number' => $_POST['mobile_number'],
    ];

    $db->closeConnection();

    if (isset($_POST['student_id'])) {
        $fillable = array_merge($fillable, ['student_id' => $_POST['student_id']]);
    }

    $contact_id = $contact->create($fillable);

    if ($contact_id > 0) {
        header('Location: ../src/views/contacts/store.php?action=create&success=true');
    } else {
        header('Location: ../src/views/contacts/store.php?action=create&success=false');
    }
}

function updateContact(Contact $teacher, Database $db)
{

}

function deleteContact(Contact $teacher, Database $db)
{

}

?>