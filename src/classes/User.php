<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL);
class User
{
    private $id;
    private $first_name;
    private $last_name;
    private $address;
    private $username; // used for login
    private $title;
    private $date_of_birth;

    protected $fillable = [
        'first_name',
        'last_name',
        'address',
        'username',
        'title',
        'date_of_birth'
    ];

    /**
     * GETTERS
     */
    protected function getId() 
    {
        return $this->id;
    }

    protected function getFirstName() 
    {
        return $this->first_name;
    }

    protected function getLastName() 
    {
        return $this->last_name;
    }

    protected function getFullName() 
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    protected function getTitle() 
    {
        return $this->title;
    }

    protected function getUsername() 
    {
        $this->username;
    }

    protected function getDateOfBirth() 
    {
        $this->date_of_birth;
    }

    public static function getAlert(string $success, string $message = null)
    {
        switch ($success) {
            case "true": 
                return '<div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Well done!</strong> Resource created successfully.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>';
            case "false": 
                return '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Oops!</strong> Something bad happened.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>';
        }
               
    }

    /**
     * SETTERS
     * 
     * NOTE: sanitize input
     */
    protected function setId(string $id)
    {
        $this->id = (int) $id;
    }

    protected function setFirstName(string $first_name) 
    {
        $this->first_name = substr($first_name, 0, 255);
    }

    protected function setLastName(string $last_name) 
    {
        $this->last_name = substr($last_name, 0, 255);
    }

    protected function setTitle(string $title) 
    {
        $this->title = substr($title, 0, 255);
    }

    protected function setUsername(string $username) 
    {
        $this->username = substr($username, 0, 255);
    }

    protected function setDateOfBirth(string $date_of_birth) 
    {
        $this->date_of_birth = substr($date_of_birth, 0, 255);
    }

    /**
     * CRUD
     */

    public static function index(array $fields, string $table_name, $conn)
    {
        try {
            $columns = array_values($fields);
            $columns_list = implode(',', $columns);
            $statement = $conn->prepare("SELECT $columns_list FROM $table_name");
            $statement->execute();
            
            return $statement;
        } catch (PDOException $e) {
            print $e->getMessage();

            return -1;
        }
    }

    /**
     * Create model in database
     */
    public function create(array $object_map, $conn = null, string $table_name = null)
    {
        try {
            $columns = array_keys($object_map);
            $columns_list = implode(',', $columns);
            $value_list = ':' . implode(', :', $columns);

            $statement = $conn->prepare("INSERT INTO $table_name ($columns_list) VALUES ($value_list)");
            $statement->execute($object_map);
            $user_id = $conn->lastInsertId();

            return $user_id;
        } catch (PDOException $e) {
            print $e->getMessage();

            return -1;
        }
    }
    
    /**
     * Create relationships
     * 
     * @param array $relationship Muste have two keys: first_key and second_key
     * the value of the first_key will be related to the second_key value.
     * @param mixed|null $conn
     * @param string $table_name name of the table where the reationship is stored.
     * 
     * @return true if sccuess; -1 if fails.
     */
    public function attach(array $relationship, $conn = null, string $table_name = null)
    {   
        try {
            $columns = array_keys($relationship);
            $columns_list = implode(',', $columns);
            $value_list = ':' . implode(', :', $columns);
           
            $statement = $conn->prepare("INSERT INTO $table_name ($columns_list) VALUES ($value_list)");

            $keys = array_keys($relationship);
            $relate = $keys[0];
            $relate_to = $keys[1];
            
            for($i = 0; $i < count($relationship[$relate_to]); $i++) {
                $statement->execute([
                    $relate => $relationship[$relate],
                    $relate_to => $relationship[$relate_to][$i]
                ]);
            }

            return true;
        } catch (PDOException $e) {
            print $e->getMessage();

            return -1;
        }
    }

    /**
     * Removes relationships
     */
    public function detach(array $object_map, $conn = null, string $table_name = null)
    {
        try {
            $result = array_map(function ($value, $key) {
                return "$key = '$value'";
            }, $object_map, array_keys($object_map));
            $condition = implode(' AND ', $result );
            
            $statement = $conn->prepare("DELETE FROM $table_name WHERE $condition");
            $statement->execute($object_map);
            
            return true;
        } catch (PDOException $e) {
            print $e->getMessage();

            return -1;
        }
    }
}

// include('config/Database.php');
// $db = new Database();
// $conn = $db->getConnection();
// $columns = [
//     'id',
//     'first_name',
//     'last_name'
// ];

// $statement = User::index($columns, 'contacts', $conn);
// for ($i = 0; $i < $statement->rowCount(); $i++) {
//     print_r($statement->fetch(PDO::FETCH_ASSOC));
// }
?>