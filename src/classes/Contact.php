<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL);
class Contact extends User
{
    private $conn;
    private $email;
    private $mobile_number;
    private $student_id;

    protected $table_name = 'contacts';
    protected $fillable = [
        'email',
        'mobile_number'
    ];

    public function __construct($db)
    {
        $this->conn = $db;
        $user = new User($db);
        $this->fillable = array_merge($user->fillable, $this->fillable);
    }

    /**
     * GETTERS
     */
    private function getEmail()
    {
        return $this->email;
    }

    private function getMobileNumber()
    {
        return $this->mobile_number;
    }

    private function getStudentId()
    {
        return $this->student_id;
    }

    /**
     * SETTERS
     * 
     * NOTE: sanitize input + validation
     */
    private function setEmail(string $email)
    {
        $this->email = substr($email, 0, 255);
    }

    private function setMobileNumber(string $mobile_number)
    {
        $this->mobile_number = substr($mobile_number, 0, 255);
    }
    
    /**
     * CRUD
     */
    public function create(array $record, $conn = null, string $table_name = null)
    {
        $this->setFirstName($record['first_name']);
        $this->setLastName($record['last_name']);
        $this->setTitle($record['title']);
        $this->setEmail($record['email']);
        $this->setMobileNumber($record['mobile_number']);

        $object_map = [
            'first_name' => $this->getFirstName(),
            'last_name' => $this->getLastName(),
            'title' => $this->getTitle(),
            'email' => $this->getEmail(),
            'mobile_number' => $this->getMobileNumber(),
        ];

        $contact_id = parent::create($object_map, $this->conn, $this->table_name);

        if (isset($record['student_id'])) {
            $this->id = $contact_id;

            $object_map = [
                'contact_id' => $contact_id,
                'student_id' => $record['student_id'],
            ];

            parent::attach($object_map, $this->conn, 'student_has_contacts');
        }

        return $contact_id;
    }

    public function attach(array $record, $conn = null, string $table_name = null)
    {
        if (isset($record['student_id']) && isset($record['contact_id'])) {
            $this->setStudentId($record['student_id']);
            $this->setId($record['contact_id']);

            $object_map = [
                'contact_id' => $this->getId(),
                'student_id' => $this->getStudentId(),
            ];

            parent::attach($object_map, $this->conn, 'student_has_contacts');
        }
    }

    public function detach(array $record, $conn = null, string $table_name = null)
    {
        if (isset($record['student_id']) && isset($record['contact_id'])) {
            $this->setStudentId($record['student_id']);
            $this->setId($record['contact_id']);

            $object_map = [
                'student_id' => $this->getStudentId(),
                'contact_id' => $this->getId(),
            ];

            parent::detach($object_map, $this->conn, 'student_has_contacts');
        }
    }
}

// $db_class = new Database();
// $db = $db_class->getConnection();
// $obj = new Contact($db);
// $result = $obj->create([
//     'first_name' => 'Aunt',
//     'last_name' => 'May',
//     'title' => 'Ms.',
//     'email' => 'email_address',
//     'mobile_number' => '666',
//     'student_id' => 3,
// ]);

// $obj->attach([
//     'student_id' => 3,
//     'contact_id' => 6,
// ]);
?>