<?php
class Teacher extends User
{
    private $conn;
    private $table_name = 'contacts';

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function isAContact(int $id): bool 
    {
        try {
            $statement = $this->conn->query("SELECT is_teacher FROM $this->table_name WHERE id = $id");
            $result = $statement->fetch(PDO::FETCH_ASSOC);

            return ((int) $result['is_teacher'] == true);
        } catch (PDOException $e) {
            print $e->getMessage();
        }
    }

    public static function index(array $fields, string $table_name, $conn)
    {
        try {
            $columns = array_values($fields);
            $columns_list = implode(',', $columns);
            $statement = $conn->prepare("SELECT $columns_list FROM $table_name WHERE is_teacher = 1");
            $statement->execute();
            
            return $statement;
        } catch (PDOException $e) {
            print $e->getMessage();

            return -1;
        }
    }

    public function create(array $record, $conn = null, string $table_name = null)
    {
        $this->setFirstName($record['first_name']);
        $this->setLastName($record['last_name']);
        $this->setTitle($record['title']);

        $object_map = [
            'first_name' => $this->getFirstName(),
            'last_name' => $this->getLastName(),
            'title' => $this->getTitle(),
            'is_teacher' => true,
        ];

        return parent::create($object_map, $this->conn, $this->table_name);
    }
}

// $db_class = new Database();
// $db = $db_class->getConnection();
// $teacher = new Teacher($db);
// $result = $teacher->create([
//     'first_name' => 'Faaree',
//     'last_name' => 'Maaan',
//     'title' => 'Mraa.',
// ]);
// var_dump($teacher->isAContact(1));

// var_dump($result);
?>