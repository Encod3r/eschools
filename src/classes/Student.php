<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL);
class Student extends User
{
    private $conn;
    private $table_name = 'students';
    private $teacher_id;
    private $contact_id;

    protected $fillable = [
        'teacher_id',
    ];

    public function __construct($db)
    {
        $this->conn = $db;
        $user = new User($db);
        $this->fillable = array_merge($user->fillable, $this->fillable);
    }

    /**
     * GETTERS
     */

    private function getTeacherId(): int
    {
        return $this->teacher_id;
    }

    private function getContactId(): array
    {
        return $this->contact_id;
    }

    /**
     * SETTERS
     * 
     * TODO: input sanitize + validation
     */

    private function setTeacherId(string $id) 
    {
        $this->teacher_id = (int) $id;
    }

    private function setContactId(array $contacts_id) 
    {
        $this->contact_id = (array) $contacts_id;
    }

    /**
     * CRUD
     */

    public function create(array $record, $conn = null, string $table_name = null)
    {
        $this->setFirstName($record['first_name']);
        $this->setLastName($record['last_name']);
        $this->setTitle($record['title']);
        $this->setTeacherId($record['teacher_id']);
         
        $object_map = [
            'first_name' => $this->getFirstName(),
            'last_name' => $this->getLastName(),
            'title' => $this->getTitle(),
            'teacher_id' => $this->getTeacherId(),
        ];

        $student_id = parent::create($object_map, $this->conn, $this->table_name);

        if (isset($record['contact_id'])) {
            $this->setContactId($record['contact_id']);
            $this->id = $student_id;

            $object_map = [
                'student_id' => $student_id,
                'contact_id' => $this->getContactId(),
            ];

            parent::attach($object_map, $this->conn, 'student_has_contacts');
        }

        return $student_id;
    }

    public function attach(array $record, $conn = null, string $table_name = null)
    {
        if (isset($record['contact_id']) && isset($record['student_id'])) {
            $this->setContactId($record['contact_id']);
            $this->setId($record['student_id']);

            $object_map = [
                'student_id' => $this->getId(),
                'contact_id' => $this->getContactId(),
            ];

            parent::attach($object_map, $this->conn, 'student_has_contacts');
        }
    }

    public function detach(array $record, $conn = null, string $table_name = null)
    {
        if (isset($record['contact_id']) && isset($record['student_id'])) {
            $this->setContactId($record['contact_id']);
            $this->setId($record['student_id']);

            $object_map = [
                'student_id' => $this->getId(),
                'contact_id' => $this->getContactId(),
            ];

            parent::detach($object_map, $this->conn, 'student_has_contacts');
        }
    }
}

// $db_class = new Database();
// $db = $db_class->getConnection();
// $student = new Student($db);
// $student->attach([
//     'student_id' => 3,
//     'contact_id' => [10,6,7],
// ]);
// $result = $student->create([
//     'first_name' => 'Peter',
//     'last_name' => 'Parker',
//     'title' => 'Mr.',
//     'teacher_id' => 6,
//     'contact_id' => 6,
// ]);
?>